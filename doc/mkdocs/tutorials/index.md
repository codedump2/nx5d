Tutorials
=========

 - [ScanSource HOWTO](tutorials/scansource-howto/scansource-howto.md): an early-phase tutorial centered around
   what today is the `ScanSource` module (fairly out-of-date by now, here just for the sake of testing
   documentation formatting.)
   
 - [KMC3 Data Processing HOWTO](tutorials/kmc3-data-processing-howto/kmc3-data-processing-howto.md):
   a data analysis tutorial prototype based in Jupyter Lab, for the KMC3-XPP beamline as BESSY II.
   Work in progress, but should be functional.
